/* Generated By:JJTree: Do not edit this line. ASTident_list.java Version 6.1 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class ASTident_list extends SimpleNode {
  public ASTident_list(int id) {
    super(id);
  }

  public ASTident_list(HL p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(HLVisitor visitor, Object data) throws Exception {

    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=bad03984daefcba9ee1897c7d4ea6e12 (do not edit this line) */
