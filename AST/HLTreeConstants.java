/* Generated By:JavaCC: Do not edit this line. HLTreeConstants.java Version 6.1_2 */
public interface HLTreeConstants
{
  public int JJTVOID = 0;
  public int JJTBODY = 1;
  public int JJTCLAUSE = 2;
  public int JJTSIMPLE_DECL = 3;
  public int JJTINT = 4;
  public int JJTSTR = 5;
  public int JJTLST = 6;
  public int JJTANY = 7;
  public int JJTIDENT_LIST = 8;
  public int JJTFN_DECL = 9;
  public int JJTPARAM_DECLS = 10;
  public int JJTNOTHING = 11;
  public int JJTPARAM_DECL = 12;
  public int JJTIF_STAT = 13;
  public int JJTFOR_STAT = 14;
  public int JJTWHILE_STAT = 15;
  public int JJTASSIGNMENT = 16;
  public int JJTINDEXED = 17;
  public int JJTFN_CALL = 18;
  public int JJTPARAMETERS = 19;
  public int JJTPRINT = 20;
  public int JJTRETURN_STAT = 21;
  public int JJTOR = 22;
  public int JJTAND = 23;
  public int JJTNOT = 24;
  public int JJTCOMPARISON = 25;
  public int JJTLT = 26;
  public int JJTLE = 27;
  public int JJTGT = 28;
  public int JJTGE = 29;
  public int JJTNE = 30;
  public int JJTEQ = 31;
  public int JJTIN = 32;
  public int JJTSUM = 33;
  public int JJTMINUS = 34;
  public int JJTPLUS = 35;
  public int JJTPROD = 36;
  public int JJTDIV = 37;
  public int JJTMOD = 38;
  public int JJTIDENTIFIER = 39;
  public int JJTINTEGER = 40;
  public int JJTSTRING = 41;
  public int JJTLIST = 42;


  public String[] jjtNodeName = {
    "void",
    "body",
    "clause",
    "simple_decl",
    "INT",
    "STR",
    "LST",
    "ANY",
    "ident_list",
    "fn_decl",
    "param_decls",
    "NOTHING",
    "param_decl",
    "if_stat",
    "for_stat",
    "while_stat",
    "assignment",
    "indexed",
    "fn_call",
    "parameters",
    "print",
    "return_stat",
    "or",
    "and",
    "not",
    "comparison",
    "LT",
    "LE",
    "GT",
    "GE",
    "NE",
    "EQ",
    "IN",
    "sum",
    "minus",
    "plus",
    "prod",
    "div",
    "mod",
    "Identifier",
    "Integer",
    "String",
    "list",
  };
}
/* JavaCC - OriginalChecksum=20b51b0633a64693581f3ac2649a3961 (do not edit this line) */
