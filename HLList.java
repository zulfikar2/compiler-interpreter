import java.util.*; 
public class HLList extends HLObject {
	
	private ArrayList<HLObject> lst;
	private Boolean negated;
	
	public HLList() {
		lst = new ArrayList<HLObject>();
		negated = false;
	}
	
	public HLList(ArrayList<HLObject> objLst) {
		lst = objLst;
		negated = false;
	}

	public String toString() {
		return lst.toString();
	}

	public Boolean equals(HLObject obj) {
		Boolean result = false;
		if(obj.getClass().getName() == "HLList") {
			if(lst.size() == 0 && obj.getList().size() == 0) {
				return true;
			}
			if(lst.size() != obj.getList().size()) {
				return false;
			}
			for(int x = 0; x < lst.size(); x++) {
				if(elementRetrieval(x).equals(obj.elementRetrieval(x))) {
					result = true;
				} 
				else {
					return false;
				}
			}
			//return lst.toString().equals(obj.getList().toString());
		}
		return result;
	}
	
	public ArrayList<HLObject> getList() {
		return lst;
	}

	public void setList(ArrayList<HLObject> objLst) {
		lst = objLst;
	}

	public Boolean getNeg() {
		return negated;
	}
	
	public HLObject negate() {
		negated = true;
		return this;
    }

	public Boolean contains(HLObject element) {
		for(int i = 0; i < lst.size(); i++) {
			if(elementRetrieval(i).equals(element)) {
				return true;
			}
		}
		return false;
	}

	public HLInteger indexOf(HLObject element) {
		int val = -1;
		if(element.getClass().getName() == "HLString") {
			if(element.contains(this)) {
				//System.out.println(lst.toString());
				val = element.toString().indexOf(lst.toString());
				val++;
			}
		}
		else {
			//System.out.println(element.getList().size());
			for(int i = 0; i < element.getList().size(); i++ ) {
				//System.out.println(i);
				try {
					if(element.elementRetrieval(i).equals(this)) {
						val = i;
						val++;
						break;
					}
				} catch (Exception e) {
				}
				
			}
		}
		if(val < 0) {
			val = 0;
		}
		return new HLInteger(val);
	}

	public HLObject elementRetrieval(int index) {
		return lst.get(index);
	}

	public HLObject intervalRetrieval(int fromindex, int toindex) {
		return null;
	}

	public HLObject add(HLObject operand) {
		if(operand.getClass().getName() == "HLList") {
			ArrayList<HLObject> lstOP = operand.getList();
			for(int i = 0; i < lstOP.size(); i++) {
				lst.add(lstOP.get(i));
			}
		}
		else {
			lst.add(operand);
		}
		return this;
	}

	public HLObject add(HLObject operand, Boolean inner) {
		if(inner) {
			lst.add(operand);
		}
		return this;
	}
	
	

	public HLObject sub(HLObject operand) {
		ArrayList<HLObject> result = new ArrayList<HLObject>();
		Boolean add = true;
		for(int x = 0; x < lst.size(); x++) {
			for(int y = 0; y < operand.getList().size(); y++) {
				if(elementRetrieval(x).equals(operand.elementRetrieval(y))) {
					add = false;
				}
			}
			if(add) {
				//System.out.println("Added : " + elementRetrieval(x).toString());
				result.add(elementRetrieval(x));
			}
			add = true;
		}
		lst = result;
		return this;
	}

	public HLObject mul(HLObject operand) {
		//CAN CHECK OTHER TYPES HERE???
		ArrayList<HLObject> result = new ArrayList<HLObject>();
		if(operand.getClass().getName() == "HLList") {
			int s1 = lst.size();
			int s2 = operand.getList().size();
			for(int x = 0; x < s1; x++) {
				for(int y = 0; y < s2; y++) {
					HLList adder = new HLList();
					HLObject ele = elementRetrieval(x);
					adder.add(ele);
					ele = operand.elementRetrieval(y);
					adder.add(ele);
					result.add(adder);
				}
			}
		}
		else if(operand.getClass().getName() == "HLInteger") {
			for(int i = 0; i < operand.intValue(); i++) {
				for(int x = 0; x < lst.size(); x++) {
					result.add(elementRetrieval(x));
				}
			}
		}
		lst = result;
		return this;
	}

	public HLObject mod(HLObject operand) {
		if(operand.getClass().getName() == "HLInteger") {
			HLList result = new HLList();
			int y = lst.size(); //keeps track of remainder spot
			if(y % operand.intValue() == 0) {
				lst = new ArrayList<HLObject>();
				return this;
			}
			while(y > operand.intValue()) {
				//System.out.println("Y : " + y) ;
				y-= operand.intValue();
			}
			for(int x = lst.size() - y ;x < lst.size(); x++) {
				result.add(elementRetrieval(x));
			}
			lst = result.getList();
		}
		return this;
	}

	public HLObject div(HLObject operand) {
		if(operand.getClass().getName() == "HLInteger") {
			HLList result = new HLList();
			ArrayList<HLObject> sub = new ArrayList<HLObject>();
			HLList subHL = new HLList();
			boolean done = false;
			int ptr = 0;
			int sizeOfSub = lst.size() / operand.intValue();
			int y = 0; //keeps track of inner list count;
			//System.out.println("SIZE : " + sizeOfSub);
			//System.out.println("SIZE # : " + lst.size());
			for(int x = 0; x < lst.size(); x++ ) {
				if(sizeOfSub == lst.size()) {
					//System.out.println("Same");
					result = new HLList(lst);
					done = true;
					break;
				}
				if(ptr < sizeOfSub) {
					sub.add(elementRetrieval(x));
					//System.out.println("Adding(" + y + ") : " + elementRetrieval(x).toString());
					ptr++;
				}
				else {
					x--;
					y++;
					//System.out.println("New List Created");
					subHL = new HLList(sub);
					result.add(subHL, true);
					sub = new ArrayList<HLObject>();
					ptr = 0;
					if((lst.size() - x) < sizeOfSub) {
						break;
					}
				}
			}

			if(sizeOfSub == lst.size()) {
				lst = new ArrayList<HLObject>();
				lst.add(result);
			}
			else {
				if(lst.size() % (double)operand.intValue() == 0) {
					subHL = new HLList(sub);
					result.add(subHL, true);
				}
				lst = result.getList();
			}
		}
		return this;
	}

}