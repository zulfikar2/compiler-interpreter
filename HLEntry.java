
public class HLEntry {
    HLObject obj;
    String type;
    String ident;
    public HLEntry() {
        type = "";
        obj = null;
        ident = null;
    }

    public HLEntry(String id, HLObject hlobj, String t) {
        obj = hlobj;
        type = t;
        ident = id;
    }

    public String getType() {
        return type;
    }

    public HLObject getObj() {
        return obj;
    }

    public String getIdent() {
        return ident;
    }

    public HLEntry setObj(HLObject hlobj) {
        obj = hlobj;
        return this;
    }

    public String toString() {
        return type + " " + ident + " " + obj.toString();
    }
}