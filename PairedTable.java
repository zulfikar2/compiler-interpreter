/**
 * PairedTable: table of IndexedPairs, which are pairs with an integer index and an Object value
 */

import java.util.ArrayList;
public class PairedTable extends ArrayList {

	// INSTANCE VARIABLES AND METHODS

	public PairedTable() {
		super();
	}
	
	// containsIndex(index) returns true if the table contains an entry with index
	public Boolean containsIndex(int index) {
		return (location(index) > -1);
	}

	// location returns index into paired table of value index index
	private int location(int index) {
		int i, size;
		size = this.size();
		for (i=0; i<size; i++)
			if (getPair(i).isIndex(index)) return i;
		return -1;
	}

	// getPair(position) returns the IndexedPair at the specified position
	public IndexedPair getPair(int position) {
		return (IndexedPair)(this.get(position));
	}

	// addPair(index,value) adds an IndexedPair if there is no pair for this index
	// otherwise the pair is not added
	public void addPair(int index, Object value) {
		int i = location(index);
		if (i==-1)
			add(new IndexedPair(index,value));
	}

	// addPair(pair) adds an IndexedPair if there is no pair for the index of the pair
	// otherwise the pair is not added
	public void addPair(IndexedPair pair) {
		int i = location(pair.getIndex());
		if (i==-1)
			add(pair);
	}

	// setValue(index,value) replaces the old value in the pair specified by index
	// if there is no such pair, nothing happens
	public void setValue(int index, Object value) {
		int i = location(index);
		if (i>-1)
			getPair(i).setValue(value);
	}

	// This setValue has the same functionality, but the parameters are passed as an IndexedPair
	public void setValue(IndexedPair pair) {
		int index = pair.getIndex();
		int i = location(index);
		if (i>-1)
			set(i,pair);
	}

	// getValue(index) return the value at the specified index if it exists
	// and null otherwise
	public Object getValue(int index) {
		int i = location(index);
		if (i>-1)
			return getPair(i).getValue();
		else
			return null;
	}

}
