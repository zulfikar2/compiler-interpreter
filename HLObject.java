/**
 * HLObject is a superclass for HLInteger, HLString, and Hllist
 * @author Sophie Quigley
 */

import java.util.*; 

public class HLObject
{
  public Boolean equals(HLObject obj) {
    return null;
  }

  public Boolean getNeg() {
    return null;
  }

	public ArrayList<HLObject> getList() {
		return null;
	}
	
	public int intValue() {
		return -1;
	}

/**
 * Creates String representation of HLObject
 * @return String representation of HLObject
 */
  public String toString()
    {
    return null;
    }
  /**
   * Verifies that element is contained in HLList or HLString
   * @param element
   * @return TRUE iff element is contained in HLobject
   */
  public Boolean contains (HLObject element) 
    {
    return Boolean.FALSE;
    }
  /**
   * Finds index of element in HLList or HLString
   * @param element
   * @return index of element in HLList or HLString or 0 if element is not contained in it
   */  
  public HLInteger indexOf(HLObject element)
    {
    return HLInteger.zero;
    }
  /**
   * Retrieves a single element of HLList or HLString
   * @param index
   * @return element at specified location
   */
  public HLObject elementRetrieval(int index)
    {
    return null;
    }
/**
 * Retrieves a subset of HLList or HLstring
 * @param fromindex location of first element in subcomponent
 * @param toindex location of first element in subcomponent
 * @return subcompoenent of HLObject specified in [fromindex,toindex] interval
 */
  public HLObject intervalRetrieval(int fromindex, int toindex)
    {
    return null;
    }
/**
 * Calculates the negation of an HLInteger
 * @return 0 - HLInteger
 */
  public HLObject negate()
    {
    return null;
    }
/**
 * Add operand to HLObject
 * @param operand
 * @return HLObject + operand
 */
  public HLObject add(HLObject operand)
    {
    return null;
    }
/**
 * Subtract operand from HLObject
 * @param operand
 * @return HLObject - operand
 */
  public HLObject sub(HLObject operand)
    {
    return null;
    }
/**
 * Multiplies HLObject by operand
 * @param operand
 * @return HLObject * operand
 * @param operand
 * @return 
 */
  public HLObject mul(HLObject operand)
    {
    return null;
    }
/**
 * Calculates remainder of HLObject divided by operand 
 * @param operand
 * @return remainder of HLObject / operand
 */
  public HLObject mod(HLObject operand)
    {
    return null;
    }
/**
 * Calculates quotient of HLObject divided by operand 
 * @param operand
 * @return quotient of HLObject / operand
 */
  public HLObject div(HLObject operand)
    {
    return null;
    }

}