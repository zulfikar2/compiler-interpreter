/**
 * IndexedPairs: these are pairs where
 * - the first element is an integer called "index" representing an index
 * - the second element is an Object called "value" representing a value
 */

public class IndexedPair {


	// INSTANCE VARIABLES AND METHODS

	private int index;
	private Object value;

	public IndexedPair(int index, Object value) {
		this.index = index;
		this.value = value;
	}

	public Boolean isIndex(int index) {
		return (this.index == index);
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public int getIndex() {
		return index;
	}

	public Object getValue() {
		return value;
	}

}
