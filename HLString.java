import java.util.regex.*;
import java.util.*; 

public class HLString extends HLObject {

	private String value;

	public HLString() {
		value = new String();
	}

	public HLString(String str) {
		value = new String(str);
	}

	public HLString(char x) {
		value = new String(x + "");
	}

	public String toString() {
		return value;
	}

	public HLString negate() {
		value = new String("-" + value);
		return this;
	}

	public Boolean equals(HLObject obj) {
		if(obj.getClass().getName() == "HLString") {
			return value.equals(obj.toString());
		}
		return false;
	}

	public Boolean contains(HLObject element) {
		return value.contains(element.toString());
	}

	public HLInteger indexOf(HLObject element) {
		int val = -1;
		if(element.getClass().getName() == "HLString") {
			if(element.contains(this)) {
				val = element.toString().indexOf(value);
				val++;
			}
		}
		else {
			if(element.contains(this)) {
				//System.out.println(element.getList());
				//System.out.println(this);
				ArrayList<HLObject> lst = element.getList();
				for(int i = 0; i < lst.size(); i++) {
					if(lst.get(i).equals(this)) {
						val = i;
						break;
					}
				}
				val++;
			}
		}
		if(val < 0) {
			val = 0;
		}
		return new HLInteger(val);
	}

	public HLObject elementRetrieval(int index) {
		value = value.charAt(index) + "";
		return this;
	}

	public HLObject intervalRetrieval(int fromindex, int toindex) {
		value = value.substring(fromindex, toindex);
		return this;
	}

	public HLObject add(HLObject operand) {
		value = value.concat(operand.toString());
		return this;
	}

	public HLObject sub(HLObject operand) {
		String str;
		if(operand.toString().charAt(0) != '-') {
			str = operand.toString();
		}
		else {
			str = operand.toString().substring(1, operand.toString().length());
		}
		//System.out.println(str);
		if (value.contains(str)) {
			value = value.replaceFirst(Pattern.quote(str), "");
		}
		return this;
	}

	public HLObject mul(HLObject operand) {
		String val = value;
		if(operand.getClass().getName() == "HLInteger") {
			if(operand.intValue() == 0) {
				value = "";
				return this;
			}
			for(int i = 0; i < operand.intValue()-1; i++) {
				value = value.concat(val);
			}
		}
		return this;
	}

	public HLObject mod(HLObject operand) {
		if(operand.getClass().getName() == "HLInteger") {
			int ptr = 0;
			int sizeOfSub = value.length() / operand.intValue();
			//System.out.println("L : " + value.length());
			for(int x = 0; x < operand.intValue(); x++) {
				ptr+=sizeOfSub;
				//System.out.println("Ptr : " + ptr);
			}
			if(ptr < value.length()) {
				value = value.substring(ptr, value.length());
			}
			else {
				value = "";
			}
		}
		return this;
	}

	public HLObject div(HLObject operand) {
		if(operand.getClass().getName() == "HLInteger") {
			HLList lst = new HLList();
			int ptr = 0;
			int sizeOfSub = value.length() / operand.intValue();
			for(int x = 0; x < operand.intValue(); x++) {
				lst.add(new HLString((value.substring(ptr, ptr+sizeOfSub))));
				ptr+=sizeOfSub;
			}

			value = lst.toString();
		}
		return this;
	}
}