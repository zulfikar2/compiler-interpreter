import java.math.*;
import java.util.*; 

public class HLInteger extends HLObject {

	Integer value;
	public static HLInteger zero = new HLInteger(0);

	public String getType() {
		return "HLInteger";
	}

	public HLInteger(Integer num) {
		value = num;
	}

	public HLInteger(int num) {
		value = new Integer(num);
	}

	public int intValue() {
		return value.intValue();
	}

	public HLInteger indexOf(HLObject element) {
		int val = -1;
		if(element.getClass().getName() == "HLString") {
			if(element.contains(this)) {
				val = element.toString().indexOf(value.toString());
				val++;
			}
		}
		else {
			if(element.contains(this)) {
				//System.out.println(element.getList());
				//System.out.println(this);
				ArrayList<HLObject> lst = element.getList();
				for(int i = 0; i < lst.size(); i++) {
					if(lst.get(i).equals(this)) {
						val = i;
						break;
					}
				}
				val++;
			}
		}
		if(val < 0) {
			val = 0;
		}
		return new HLInteger(val);
	}

	public String toString() {
		return value.toString();
	}

	public Boolean equals(HLObject obj) {
		if(obj.getClass().getName() == "HLInteger") {
			return intValue() == obj.intValue();
		}
		return false;
	}

	public HLInteger negate() {
		value = new Integer(0 - value.intValue());
		return this;
	}

	public HLObject add(HLObject obj) {
		value = new Integer(value.intValue() + ((HLInteger) obj).intValue());
		return this;
	}

	public HLObject sub(HLObject obj) {
		value = new Integer(value.intValue() + ((HLInteger) obj).intValue());
		return this;
	}

	public HLObject mul(HLObject obj) {
		value = new Integer(value.intValue() * ((HLInteger) obj).intValue());
		return this;
	}

	public HLObject div(HLObject obj) {
		value = new Integer(value.intValue() / ((HLInteger) obj).intValue());
		return this;
	}

	public HLObject mod(HLObject obj) {
		value = new Integer(value.intValue() % ((HLInteger) obj).intValue());
		return this;
	}

}